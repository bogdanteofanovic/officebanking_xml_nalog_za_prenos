package main;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.Dimension;
import java.io.FileWriter;
import java.io.IOException;

public class Main extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JTextField tfNazivKomitenta;
	private JTextField tfAdresaKomitenta;
	private JTextField tfRacunKomitenta;
	private JTextField tfSifraPlacanja;
	private JTextField tfIznos;
	private JTextField taSvrhaPlacanja;
	private JTextField tfModel;
	private JTextField tfPozivNaBroj;
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dbBuilder;
	private Document doc;
	
	
	public Main() {
		
		
		init();
	}
	
	
	private void init() {
		setTitle("XML Nalog");
		setSize(new Dimension(450, 330));
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAkcije = new JMenu("Akcije");
		menuBar.add(mnAkcije);
		
		JMenuItem mntmIzlaz = new JMenuItem("Izlaz");
		mntmIzlaz.setHorizontalTextPosition(SwingConstants.CENTER);
		mntmIzlaz.setHorizontalAlignment(SwingConstants.CENTER);
		mnAkcije.add(mntmIzlaz);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[][grow]", "[][][][][][][grow][][][][]"));
		
		JLabel lblNazivKomitenta = new JLabel("Naziv komitenta:");
		panel.add(lblNazivKomitenta, "cell 0 0,alignx trailing");
		
		tfNazivKomitenta = new JTextField();
		panel.add(tfNazivKomitenta, "cell 1 0,growx");
		tfNazivKomitenta.setColumns(10);
		
		JLabel lblAdresaKomitenta = new JLabel("Adresa komitenta:");
		panel.add(lblAdresaKomitenta, "cell 0 1,alignx trailing");
		
		tfAdresaKomitenta = new JTextField();
		panel.add(tfAdresaKomitenta, "cell 1 1,growx");
		tfAdresaKomitenta.setColumns(10);
		
		JLabel lblFormatAdreseUlica = new JLabel("Format adrese Ulica, Postanski broj, Grad");
		panel.add(lblFormatAdreseUlica, "cell 1 2,alignx center");
		
		JLabel lblRacunKomitenta = new JLabel("Racun komitenta:");
		panel.add(lblRacunKomitenta, "cell 0 3,alignx trailing");
		
		tfRacunKomitenta = new JTextField();
		panel.add(tfRacunKomitenta, "cell 1 3,growx");
		tfRacunKomitenta.setColumns(10);
		
		JLabel lblsifraPlacanja = new JLabel("\u0160ifra pla\u0107anja:");
		panel.add(lblsifraPlacanja, "cell 0 5,alignx trailing");
		
		tfSifraPlacanja = new JTextField();
		panel.add(tfSifraPlacanja, "cell 1 5,alignx left");
		tfSifraPlacanja.setColumns(10);
		
		JLabel lblSvrhaPlacanja = new JLabel("Svrha pla\u0107anja:");
		panel.add(lblSvrhaPlacanja, "cell 0 6,alignx right");
		
		taSvrhaPlacanja = new JTextField();
		taSvrhaPlacanja.setBorder(UIManager.getBorder("TextField.border"));
		panel.add(taSvrhaPlacanja, "cell 1 6,grow");
		
		JLabel lblModelIPoziv = new JLabel("Model i poziv na broj:");
		panel.add(lblModelIPoziv, "cell 0 7,alignx trailing");
		
		tfModel = new JTextField();
		panel.add(tfModel, "flowx,cell 1 7,alignx left");
		tfModel.setColumns(10);
		
		
		
		JLabel lblIznos = new JLabel("Iznos:");
		panel.add(lblIznos, "cell 0 8,alignx trailing");
		
		tfIznos = new JTextField();
		panel.add(tfIznos, "cell 1 8,alignx left");
		tfIznos.setColumns(10);
		
		JButton btnKreiraj = new JButton("Napravi XML");
		btnKreiraj.addActionListener(e-> {
			JFileChooser jfc = new JFileChooser();
			createXml();
		    if( jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ){
		    	try {
		    		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		            Transformer transformer = transformerFactory.newTransformer();
		    		DOMSource source = new DOMSource(doc);
		    		StreamResult result = new StreamResult(new FileWriter(jfc.getSelectedFile()+".xml"));
		            transformer.transform(source, result);
		    		
	                JOptionPane.showMessageDialog(null, "File has been saved","File Saved",JOptionPane.INFORMATION_MESSAGE);
	                // true for rewrite, false for override

	            } catch (IOException e1) {
	                e1.printStackTrace();
	            } catch (TransformerException e1) {
					e1.printStackTrace();
				}
		    }
		});
		panel.add(btnKreiraj, "cell 1 10,alignx right");
		
		tfPozivNaBroj = new JTextField();
		panel.add(tfPozivNaBroj, "cell 1 7,growx");
		tfPozivNaBroj.setColumns(10);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}
	
	private void createXml() {
		
		try {
			
			
			dbFactory = DocumentBuilderFactory.newInstance();
			dbBuilder = dbFactory.newDocumentBuilder();
			doc = dbBuilder.newDocument();
			
			Element pmtorderrqElement = doc.createElement("pmtorderrq");
			doc.appendChild(pmtorderrqElement);
			
			Element pmtorderElement = doc.createElement("pmtorder");
			pmtorderrqElement.appendChild(pmtorderElement);
			
			Element companyinfoElement = doc.createElement("companyinfo");
			pmtorderElement.appendChild(companyinfoElement);
			
			Element nameElement = doc.createElement("name");
			Element cityElement = doc.createElement("city");
			
			companyinfoElement.appendChild(nameElement);
			companyinfoElement.appendChild(cityElement);
			
			nameElement.appendChild(doc.createTextNode("Pico"));
			cityElement.appendChild(doc.createTextNode("Save Djisalova 2d,21000, Novi Sad"));
			
			//account info
			Element accountinfoElement = doc.createElement("accountinfo");
			pmtorderElement.appendChild(accountinfoElement);
			
			Element acctidElement = doc.createElement("acctid");
			acctidElement.appendChild(doc.createTextNode("285-2211050000465-67"));
			accountinfoElement.appendChild(acctidElement);
			
			//bankid element missing
			
			Element banknameElement = doc.createElement("bankname");
			banknameElement.appendChild(doc.createTextNode("Sberbank Srbija a.d."));
			accountinfoElement.appendChild(banknameElement);
			
			Element payeecompanyinfoElement = doc.createElement("payeecompanyinfo");
			pmtorderElement.appendChild(payeecompanyinfoElement);
			
			Element paymentName = doc.createElement("name");
			paymentName.appendChild(doc.createTextNode(tfNazivKomitenta.getText()));
			payeecompanyinfoElement.appendChild(paymentName);
			
			Element paymentCity = doc.createElement("city");
			paymentCity.appendChild(doc.createTextNode(tfAdresaKomitenta.getText()));
			payeecompanyinfoElement.appendChild(paymentCity);
			
			
			Element payeeaccountinfoElement = doc.createElement("payeeaccountinfo");
			pmtorderElement.appendChild(payeeaccountinfoElement);
			
			
			String[] racunSplit = tfRacunKomitenta.getText().split("-");
			System.out.println(13-racunSplit[1].length());
			
			StringBuilder builder = new StringBuilder("");
			
			for (int i = 0; i < (13-racunSplit[1].length()); i++) {
			    builder.append("0");
			}
			
			String racun = racunSplit[0] + builder + racunSplit[1] + racunSplit[2];
			
			System.out.println(racun);
			
			Element payeeAcctidElement = doc.createElement("acctid");
			payeeAcctidElement.appendChild(doc.createTextNode(racun));
			payeeaccountinfoElement.appendChild(payeeAcctidElement);
			
			
			Element trntypeElement = doc.createElement("trntype");
			trntypeElement.appendChild(doc.createTextNode("ibank.payment.pp3"));
			pmtorderElement.appendChild(trntypeElement);
			
			
			Element trnamtElement = doc.createElement("trnamt");
			trnamtElement.appendChild(doc.createTextNode(String.valueOf(Float.valueOf(tfIznos.getText()))));
			pmtorderElement.appendChild(trnamtElement);
			
			Element purposeElement = doc.createElement("purpose");
			purposeElement.appendChild(doc.createTextNode(taSvrhaPlacanja.getText()));
			pmtorderElement.appendChild(purposeElement);
			
			Element purposecodeElement = doc.createElement("purposecode");
			purposecodeElement.appendChild(doc.createTextNode(tfSifraPlacanja.getText()));
			pmtorderElement.appendChild(purposecodeElement);
			
			Element curdefElement = doc.createElement("curdef");
			curdefElement.appendChild(doc.createTextNode("RSD"));
			pmtorderElement.appendChild(curdefElement);
			
			Element payeerefmodelElement = doc.createElement("payeerefmodel");
			payeerefmodelElement.appendChild(doc.createTextNode(tfModel.getText()));
			pmtorderElement.appendChild(payeerefmodelElement);
			
			Element payeerefnumberElement = doc.createElement("payeerefnumber");
			payeerefnumberElement.appendChild(doc.createTextNode(tfPozivNaBroj.getText()));
			pmtorderElement.appendChild(payeerefnumberElement);
			
			Element urgencyElement = doc.createElement("urgency");
			urgencyElement.appendChild(doc.createTextNode("ACH"));
			pmtorderElement.appendChild(urgencyElement);
			
			Element priorityElement = doc.createElement("priority");
			priorityElement.appendChild(doc.createTextNode("0"));
			pmtorderElement.appendChild(priorityElement);
			
			Element propertiesElement = doc.createElement("properties");
			pmtorderElement.appendChild(propertiesElement);
			
			Element notificationElement = doc.createElement("notification");
			propertiesElement.appendChild(notificationElement);
			
			Element channelElement = doc.createElement("channel");
			channelElement.appendChild(doc.createTextNode("ibank.rc"));
			notificationElement.appendChild(channelElement);
			
			
			
			
		
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args) {
		
		try {
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Main m = new Main();

	}

}
